<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- POPUPS -->
		<div class="popup-mask js-close"></div>

		<div class="popup-container">
			<div class="close js-close"></div>
			
			<!-- INSERT POPUP HTML HERE -->
			
			<div class="popup-wrap">
				<div class="popup popup-content" id="popdev-target">
					<!-- CUSTOM HTML FROM DEV HERE -->
				</div>

				<div class="popup popup-custom" id="custom">
					<h1>I AM A POPUP</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>

				<div class="popup popup-custom" id="custom02">
					<h1>I AM A POPUP CUSTOM02</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nam consequatur reiciendis omnis esse, nesciunt!</p>

					<a href="javascript:void(0)" class="btn-main js-close">Close Me</a>
	
				</div>
			</div>
		</div>
		
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">

			<!-- content -->
			<!-- <a href="javascript:void(0)" onclick="popOpen('custom')">Trigger popup</a> -->

			<div class="sidebar-wrap">
				<div class="sidebar">
					<a class="js-btnsidenav side-nav active" href="javascript:void(0)">
						<div class="indicator">
							<div class="tag">
								<img class="sidenav-logo" src="dist/images/twitter.svg" alt="">
							</div>
							<div class="bar"></div>
						</div>
						<div class="label">Animation Reel</div>
					</a>
					<a class="js-btnsidenav side-nav" href="javascript:void(0)">
						<div class="indicator">
							<div class="tag">01</div>
							<div class="bar"></div>
						</div>
						<div class="label">Einstein Bros. Twist n’ Di</div>
					</a>
					<a class="js-btnsidenav side-nav" href="javascript:void(0)">
						<div class="indicator">
							<div class="tag">02</div>
							<div class="bar"></div>
						</div>
						<div class="label">Nike Steal The Show</div>
					</a>  
					<a class="js-btnsidenav side-nav" href="javascript:void(0)">
						<div class="indicator">
							<div class="tag">03</div>
							<div class="bar"></div>
						</div>
						<div class="label">Allstate Survival Guide</div>
					</a>  
					<a class="js-btnsidenav side-nav" href="javascript:void(0)">
						<div class="indicator">
							<div class="tag">04</div>
							<div class="bar"></div>
						</div>
						<div class="label">Ello Control</div>
					</a>  
					<a class="js-btnsidenav side-nav" href="javascript:void(0)">
						<div class="indicator">
							<div class="tag">05</div>
							<div class="bar"></div>
						</div>
						<div class="label">Nissan Zero Gravity</div>
					</a>  
					<a class="js-btnsidenav side-nav" href="javascript:void(0)">
						<div class="indicator">
							<div class="tag">06</div>
							<div class="bar"></div>
						</div>
						<div class="label">New York Times Debt Deal</div>
					</a>
					<a class="js-btnsidenav side-nav" href="javascript:void(0)">
						<div class="indicator">
							<div class="tag">07</div>
							<div class="bar"></div>
						</div>
						<div class="label">Alameda New Leaf</div>
					</a>   
				</div>				
			</div>





			<!-- legwork nav start-->
			<div class="content">
				<div class="section">logo</div>
				<div class="section">1</div>
				<div class="section">2</div>
				<div class="section">3</div>
				<div class="section">4</div>
				<div class="section">5</div>
				<div class="section">6</div>
				<div class="section">7</div>				
			</div>

			<!-- legwork nav end -->







			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>

