/*POPUP CONTROL
* <div class=".popup-mask">
*      <div class=".popupclassname .popup">POPUP</div>
* </div>
* <a rel="popupclassname" class="js-pop">POPUP TRIGGER</a>
* 
*/

/*if (jQuery('.popup-mask').length) {
  jQuery('.popup-mask').hide();

  jQuery(document).on('click', '.js-pop', function() {
    popcont = this.rel;
    jQuery('.popup-mask').fadeIn();
    jQuery('.popup').hide();
    jQuery('.' + popcont).show();
  });

  // close pop-up button
  jQuery('.popup').on('click', '.js-close', function() {
    jQuery(this).closest('.popup-mask').fadeOut();
  });
}*/

function check_pop_ht() {
  var window_ht,
      pop_wrap_ht;

  window_ht = $(window).height();
  pop_cont_ht = $('.popup-wrap').height();

  if (pop_cont_ht > window_ht) {
    $('.popup-container').css({
      'height' : '100%'
    });
    enquire.register("screen and (min-width: 500px)", {
      match : function() {
        $('.popup-container').perfectScrollbar();
      },
      unmatch : function() {
        $('.popup-container').perfectScrollbar('destroy');
      }
    });
  }
  else {
    $('.popup-container').css({
      'height' : 'auto'
    });
  }
}

// show popup "popOpen(id) or popOpen()"
var pop_default_id = "popdev-target";

function popOpen(id) {
  $('.popup-mask, .popup-container, .js-close').fadeIn();
  if (id == null) {
    $('#'+pop_default_id).fadeIn();
  }
  else {
    $('#'+id).fadeIn();
  }
  check_pop_ht();
}

$(window).resize(function(){
  check_pop_ht();
});

// hide popup "popClose()"
function popClose() {

  $('#'+pop_default_id).empty();
  $('.popup-mask, .popup-container, .popup').fadeOut();
}

$(document).on('click', '.js-close', function(){
  popClose();
});






$(document).ready(function(){
  var wheel = 0;
  var section = $('.section').length-1;
  console.log(section);


function selectSection() {
    $('.section').hide();
    $('.section').eq(wheel).fadeIn();
    $('.side-nav').removeClass('active');
    $('.side-nav').eq(wheel).addClass('active');
    // $('.section').eq(wheel).show();      
}

// wheel
function wheel_fnx(e) {
console.log(e.deltaY);
    if (e.deltaY >= 1) {
      console.log('down '+e.deltaY);
      if (wheel >= section) {
        console.log('limitdown');
      }
      else {
        wheel += 1;
        console.log(wheel);   
        selectSection();
        wheel_off();
        setTimeout(wheel_on, 500);
      }

    }
    else if (e.deltaY < 0) {
      console.log('up '+e.deltaY);
      if (wheel <= 0) {
        console.log('limitup');
      }
      else {
        wheel -= 1;
        console.log(wheel);
        selectSection();
        wheel_off();
        setTimeout(wheel_on, 500);  
      }
    }

    else {}
}


function wheel_on() {
  document.querySelector('.content').addEventListener('wheel', wheel_fnx);
}

function wheel_off() {
   document.querySelector('.content').removeEventListener('wheel', wheel_fnx);
}

wheel_on();


// swipe
$('.content').on('swipedown', function(){

  if (wheel <= 0) {
    console.log('limitup');
  }
  else {
    wheel -= 1;
    console.log(wheel);
    selectSection(); 
  }

});

$('.content').on('swipeup', function(){

  if (wheel >= section) {
    console.log('limitdown');
  }
  else {
    wheel += 1;
    console.log(wheel);   
    selectSection();
  }

});





// click
$('.js-btnsidenav').click(function(){
  var click = $(this).index();
  wheel = click;
  selectSection();
});



  document.addEventListener("drag", function( event ) {
    alert('test');
  });





});